## 명령 종류
- **데이터 처리 명령어**
- 메모리 접근 명령어
- 분기 명령어
- 기타 명령어
- 상태 플래그와 실행 조건 코드

* * *

## 데이터 처리 명령어
- 산술 연산
- 논리 연산
- Register 값 저장
- 비교

* * *

## 산술 연산
  |명령어 및 형식|설명|
  |-|-|
  |ADD (dest), (oper1), (oper2)|dest = oper1 + oper2|
  |SUB (dest), (oper1), (oper2)|dest = oper1 - oper2|
  |adc, sbc (dest), (oper1), (oper2)|Carry를 포함한 add, sub|
  |RSB (dest), (oper1), (oper2)|dest = oper2 - oper1|
  |RSC (dest), (oper1), (oper2)|Carry를 포함한 rsb|

* * *
##  논리 연산
  |명령어 및 형식|설명|
  |-|-|
  |AND (dest), (oper1), (oper2)|dest = oper1 AND oper2|
  |ORR (dest), (oper1), (oper2)|dest = oper1 OR oper2|
  |EOR (dest), (oper1), (oper2)|dest = oper1 XOR oper2|
  |BIC (dest), (oper1), (oper2)|dest = oper1 AND NOT oper2|

* * *

## Register 값 저장
  |명령어 및 형식|설명|
  |-|-|
  |MOV (dest), (oper1)|dest = oper1|
  |MVN (dest), (oper1), (oper2)|dest = (NOT)oper1|

* * *

## 비교
  |명령어 및 형식|설명|
  |-|-|
  |CMP (dest), (oper1), (oper2)|dest = oper1 + oper2|
  |CMN (dest), (oper1), (oper2)|dest = oper1 - oper2|
  |TST (dest), (oper1), (oper2)|Carry를 포함한 add, sub|
  |TEQ (dest), (oper1), (oper2)|dest = oper2 - oper1|

* * *

## 예제 코드

#### http://heart4u.co.kr/tblog/328
#### http://electronicsdo.tistory.com/entry/CAN%ED%86%B5%EC%8B%A0%EC%9D%98-%EA%B8%B0%EC%B4%882
#### http://infocenter.arm.com/help/index.jsp
#### http://recipes.egloos.com/5618965
